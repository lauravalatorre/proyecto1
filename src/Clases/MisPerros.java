
package Clases;
public class MisPerros {
    public static void main(String[] args) {
       
        //Declarar un perro
        
        Perro Garu;
        Perro Spok;
        //Crear un perro
        
        Garu= new Perro();
        Garu.raza="Chiguagua";
        
        Spok= new Perro();
        Spok.raza="Doberman";
        //Hacer que mi perro ladre
        Garu.ladrar();
        Spok.ladrar();
    }
    
}
